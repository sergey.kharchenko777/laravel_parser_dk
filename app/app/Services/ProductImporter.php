<?php

namespace App\Services;

use App\Interfaces\ProductProviderInterface;
use App\Models\Category;
use App\Models\Product;

class ProductImporter
{
    /**
     * @var ProductProviderInterface
     */
    private $productProvider;

    /**
     * @param ProductProviderInterface $productProvider
     */
    public function __construct(ProductProviderInterface $productProvider)
    {
        $this->productProvider = $productProvider;
    }

    /**
     * Update categories and products
     */
    public function importProductsAndCategories()
    {
        foreach ($this->productProvider->getProducts() as $item) {
            $product = Product::where('code', $item['code'])->get();
        }
        if ($product) {
            $category = $this->processCategory($item['category'], $product->category());
        }
        else
        {
            $category = $this->processCategory($item['category']);
        }
    }

    /**
     * @param array $data
     * @param Category|null $category
     * @return Category
     */
    private function processCategory(array $data, Category $category = null): Category
    {
        if ($category && $category->getId() == $data['id']) {
            $categoryOfProduct = $category;
            $categoryOfProduct->setName($data['name']);
            $this->categoryProductRepository->save($categoryOfProduct, true);

            return $categoryOfProduct;
        }
        if ($category && $category->getId() != $data['id']) {
            $categoryOfProduct = $this->categoryProductRepository->findOneBy(['id' => $data['id']]);
            if (!$categoryOfProduct) {
                $categoryOfProduct = $this->createNewCategory($data['name']);
            }
        }
        else
        {
            $categoryOfProduct = $this->createNewCategory($data['name']);
        }

        return $categoryOfProduct;
    }
}
