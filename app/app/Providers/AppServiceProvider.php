<?php

namespace App\Providers;

use App\Interfaces\ProductProviderInterface;
use App\Services\JsonProductProvider;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(ProductProviderInterface::class, JsonProductProvider::class);
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
