<?php

namespace App\Interfaces;

interface ProductProviderInterface
{
    /**
     * Get product list
     *
     * @return array
     */
    public function getProducts();
}
