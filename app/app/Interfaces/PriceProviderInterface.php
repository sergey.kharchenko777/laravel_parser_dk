<?php

namespace App\Interfaces;

interface PriceProviderInterface
{
    /**
     * @return array
     */
    public function getPrices();
}
