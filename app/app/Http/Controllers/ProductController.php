<?php

namespace App\Http\Controllers;

use App\Services\ProductImporter;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ProductController extends Controller
{
    public function index(ProductImporter $productImporter): View
    {
//        $productImporter->importProductsAndCategories();

        return view('product.list');
    }

    public function importProductsAndCategories(ProductImporter $productImporter): View
    {
        return view();
    }
}
